package com.blanclink.blancgram;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void applyGrayscaleFilter(View view) {
        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        Bitmap source = null;
        int[] originalPixels = null;

        if (imageView != null) {
            source = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
        }
        if (source != null) {
            originalPixels = new int[source.getWidth() * source.getHeight()];
        }
        if (originalPixels != null) {
            source.getPixels(originalPixels, 0, source.getWidth(), 0, 0, source.getWidth(), source.getHeight());
            for (int i = 0; i < originalPixels.length; i++) {
                int average = ( getRedFromPixel(originalPixels[i]) +
                                getGreenFromPixel(originalPixels[i]) +
                                getBlueFromPixel(originalPixels[i])
                                ) / 3;
                int newPixel = createPixel(getAlphaFromPixel(originalPixels[i]), average, average, average);
                originalPixels[i] = newPixel;
            }
            Bitmap grayscaleBitmap =  Bitmap.createBitmap(originalPixels, source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
            imageView.setImageBitmap(grayscaleBitmap);
        }
    }

    public void applySepiaFilter(View view) {
        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        Bitmap source = null;
        int[] originalPixels = null;

        if (imageView != null) {
            source = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
        }
        if (source != null) {
            originalPixels = new int[source.getWidth() * source.getHeight()];
        }
        if (originalPixels != null) {
            source.getPixels(originalPixels, 0, source.getWidth(), 0, 0, source.getWidth(), source.getHeight());
            for (int i = 0; i < originalPixels.length; i++) {
                int alpha = getAlphaFromPixel(originalPixels[i]);
                int red = getRedFromPixel(originalPixels[i]);
                int green = getGreenFromPixel(originalPixels[i]);
                int blue = getBlueFromPixel(originalPixels[i]);
                int newPixel = createPixel(alpha,
                        (int)Math.min(255, (red * 0.393) + (green * 0.769) + (blue * 0.189)),
                        (int)Math.min(255, (red * 0.349) + (green * 0.686) + (blue * 0.168)),
                        (int)Math.min(255, (red * 0.272) + (green * 0.534) + (blue * 0.131)));
                originalPixels[i] = newPixel;
            }
            Bitmap grayscaleBitmap =  Bitmap.createBitmap(originalPixels, source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
            imageView.setImageBitmap(grayscaleBitmap);
        }
    }

    /**
     * TODO: Implement invertColorsFilter where each color component (red, green and blue) will be the result of 255 minus its current value
     */
    private static int getAlphaFromPixel(int pixel) { return pixel >> 24 & 0xFF; }
    private static int getRedFromPixel(int pixel) { return pixel >> 16 & 0xFF; }
    private static int getGreenFromPixel(int pixel) { return pixel >> 8 & 0xFF; }
    private static int getBlueFromPixel(int pixel) { return pixel & 0xFF; }
    private static int createPixel(int alpha, int red, int green, int blue) {
        return (alpha << 24) + (red << 16) + (green << 8) + blue;
    }
}
